## PerfCounter

[![pipeline  status](https://gitlab.com/prime-hack/samp/plugins/perfcounter/badges/main/pipeline.svg)](https://gitlab.com/prime-hack/samp/plugins/perfcounter/-/commits/main)

This plugin is a performance counter for *GTA: San Andreas*

![image-20210701195921552](README.assets/image-20210701195921552.png)

### [Download](https://gitlab.com/prime-hack/samp/plugins/perfcounter/-/jobs/artifacts/main/download\?job\=win32)

## Add User-defined timers

You can add custom timers via **PerfCounter.json** file.

Structure of file:

```json
{
    "Timer_Name": {
        "address": 1337,
        "size": 5,
        "module": "samp"
    }
}
```

This is base structure to add timer **Timer_Name** at address **samp.dll+1337** (size 5). You can also add children for timers:

```json
{
    "Timer_Name": {
        "address": 1337,
        "size": 5,
        "module": "samp",
        "children": {
            "Child1": {
                "address": 1337,
                "size": 5,
                "module": "samp"
            },
            "Child2": {
                "address": 1337,
                "size": 5,
                "module": "samp"
            }
        }
    }
}
```

### Example of user-defined timers

This config add 7 new timers for SA:MP 0.3.7-R3:

```json
{
    "TextdrawPool::Render": {
        "address": 809172,
        "size": 5,
        "module": "samp"
    },
    "ObjectPool::Render": {
        "address": 674211,
        "size": 5,
        "module": "samp"
    },
    "mainloop": {
        "address": 809092,
        "size": 5,
        "module": "samp",
        "children": {
            "CNetGame::Process": {
                "address": 809051,
                "size": 5,
                "module": "samp",
                "children": {
                    "CNetGame::UpdateNetwork": {
                        "address": 45705,
                        "size": 5,
                        "module": "samp"
                    },
                    "CNetGame::ProcessSync": {
                        "address": 45822,
                        "size": 5,
                        "module": "samp"
                    }
                }
            },
            "BASS::Process": {
                "address": 809066,
                "size": 5,
                "module": "samp"
            }
        }
    }
}
```

![user-defined timers](README.assets/2021-11-22_06-15.png)
