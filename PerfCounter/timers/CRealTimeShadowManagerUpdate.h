#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CRealTimeShadowManagerUpdate : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53EA0D, 5 };

public:
	CRealTimeShadowManagerUpdate( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
