#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CCoverUpdate : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53BF4E, 5 };

public:
	CCoverUpdate( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
