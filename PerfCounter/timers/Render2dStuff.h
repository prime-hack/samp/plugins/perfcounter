#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class Render2dStuff : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53EB12, 5 };

public:
	Render2dStuff( SRDescent *parent );

	std::string_view getPrefix() const override;
};
