#include "CMirrorsRenderMirrorBuffer.h"

CMirrorsRenderMirrorBuffer::CMirrorsRenderMirrorBuffer( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CMirrorsRenderMirrorBuffer::getName() const {
	return "CMirrors::RenderMirrorBuffer";
}

std::string_view CMirrorsRenderMirrorBuffer::getPrefix() const {
	return "  ";
}
