#include "CShadowsRenderStaticShadows.h"

CShadowsRenderStaticShadows::CShadowsRenderStaticShadows( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CShadowsRenderStaticShadows::getName() const {
	return "CShadows::RenderStaticShadows";
}

std::string_view CShadowsRenderStaticShadows::getPrefix() const {
	return "    ";
}
