#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CAudioEngineService : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53E98B, 5 };

public:
	CAudioEngineService( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
