#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CMirrorsBeforeMainRender : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53EA12, 5 };

public:
	CMirrorsBeforeMainRender( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
