#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CRendererRenderFadingInEntities : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53E019, 5 };

public:
	CRendererRenderFadingInEntities( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
