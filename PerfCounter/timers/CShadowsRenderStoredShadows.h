#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CShadowsRenderStoredShadows : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53E0C8, 5 };

public:
	CShadowsRenderStoredShadows( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
