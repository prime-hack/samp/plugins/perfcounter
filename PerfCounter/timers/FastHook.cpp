#include "FastHook.h"

FastHook::FastHook( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view FastHook::getName() const {
	return "SRHook::Fast";
}
