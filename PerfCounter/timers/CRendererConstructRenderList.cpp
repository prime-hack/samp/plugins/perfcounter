#include "CRendererConstructRenderList.h"

CRendererConstructRenderList::CRendererConstructRenderList( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CRendererConstructRenderList::getName() const {
	return "CRenderer::ConstructRenderList";
}

std::string_view CRendererConstructRenderList::getPrefix() const {
	return "  ";
}
