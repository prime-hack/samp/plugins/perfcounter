#include "ProcessPhysics.h"

ProcessPhysics::ProcessPhysics( SRDescent *parent ) : IPerfTimer( parent ) {
	hook_before.onBefore += [this] {
		before();
	};
	hook_before.install();
	hook_after.onBefore += [this] {
		after();
	};
	hook_after.install();
}

std::string_view ProcessPhysics::getName() const {
	return "inline::ProcessPhysics";
}

std::string_view ProcessPhysics::getPrefix() const {
	return "    "
		   "    ";
}