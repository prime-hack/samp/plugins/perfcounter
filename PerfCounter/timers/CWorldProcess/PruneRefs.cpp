#include "PruneRefs.h"

PruneRefs::PruneRefs( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view PruneRefs::getName() const {
	return "CReferences::PruneAllReferencesInWorld";
}

std::string_view PruneRefs::getPrefix() const {
	return "    "
		   "    ";
}