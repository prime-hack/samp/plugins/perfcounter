#include "AddEventsToGrp.h"

AddEventsToGrp::AddEventsToGrp( CWorldProcess *parent ) : IPerfTimer( parent ) {
	parent->onGrpEvtLoop += [this] {
		before();
	};

	hook.onBefore += [this] {
		after();
	};
	hook.install();
}

std::string_view AddEventsToGrp::getName() const {
	return "inline::AddEventsToGroup";
}

std::string_view AddEventsToGrp::getPrefix() const {
	return "    "
		   "    ";
}