#include "Cutscene.h"

Cutscene::Cutscene( SRDescent *parent ) : IPerfTimer( parent ) {
	hook_before.onBefore += [this] {
		before();
	};
	hook_before.install();
	hook_after.onBefore += [this] {
		after();
	};
	hook_after.install();
}

std::string_view Cutscene::getName() const {
	return "inline::Cutscene";
}

std::string_view Cutscene::getPrefix() const {
	return "    "
		   "    ";
}