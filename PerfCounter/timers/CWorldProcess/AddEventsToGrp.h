#pragma once

#include "CWorldProcess.h"
#include <llmo/SRHookFast.h>

class AddEventsToGrp : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x568506, 5 };

public:
	AddEventsToGrp( CWorldProcess *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
