#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class UpdateAnims : public IPerfTimer {
	SRHook::Fast::Hook hook_before{ 0x5685AA, 5 };
	SRHook::Fast::Hook hook_after{ 0x5685C4, 6 };

public:
	UpdateAnims( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
