#include "ProcessPlayers.h"

ProcessPlayers::ProcessPlayers( SRDescent *parent ) : IPerfTimer( parent ) {
	hook_before.onBefore += [this] {
		before();
	};
	hook_before.install();
	hook_after.onBefore += [this] {
		after();
	};
	hook_after.install();
}

std::string_view ProcessPlayers::getName() const {
	return "inline::ProcessPlayers";
}

std::string_view ProcessPlayers::getPrefix() const {
	return "    "
		   "    ";
}