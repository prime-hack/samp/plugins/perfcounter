#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class ProcessControl : public IPerfTimer {
	SRHook::Fast::Hook hook_before{ 0x5685C4, 6 };
	SRHook::Fast::Hook hook_after{ 0x568667, 7 };

public:
	ProcessControl( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
