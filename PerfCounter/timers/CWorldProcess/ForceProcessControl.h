#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class ForceProcessControl : public IPerfTimer {
	SRHook::Fast::Hook hook_before{ 0x568667, 7 };
	SRHook::Fast::Hook hook_after{ 0x568711, 6 };

public:
	ForceProcessControl( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
