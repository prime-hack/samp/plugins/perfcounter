#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class PruneRefs : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x568527, 5 };

public:
	PruneRefs( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
