#include "ProcessControl.h"

ProcessControl::ProcessControl( SRDescent *parent ) : IPerfTimer( parent ) {
	hook_before.onBefore += [this] {
		before();
	};
	hook_before.install();
	hook_after.onBefore += [this] {
		after();
	};
	hook_after.install();
}

std::string_view ProcessControl::getName() const {
	return "inline::ProcessControl";
}

std::string_view ProcessControl::getPrefix() const {
	return "    "
		   "    ";
}