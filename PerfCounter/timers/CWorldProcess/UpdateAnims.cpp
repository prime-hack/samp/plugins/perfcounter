#include "UpdateAnims.h"

UpdateAnims::UpdateAnims( SRDescent *parent ) : IPerfTimer( parent ) {
	hook_before.onBefore += [this] {
		before();
	};
	hook_before.install();
	hook_after.onBefore += [this] {
		after();
	};
	hook_after.install();
}

std::string_view UpdateAnims::getName() const {
	return "inline::UpdateAnims";
}

std::string_view UpdateAnims::getPrefix() const {
	return "    "
		   "    ";
}