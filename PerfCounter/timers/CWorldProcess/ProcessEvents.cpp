#include "ProcessEvents.h"

ProcessEvents::ProcessEvents( SRDescent *parent ) : IPerfTimer( parent ) {
	hook_before.onBefore += [this] {
		before();
	};
	hook_before.install();
	hook_after.onBefore += [this] {
		after();
	};
	hook_after.install();
}

std::string_view ProcessEvents::getName() const {
	return "inline::ProcessEvents";
}

std::string_view ProcessEvents::getPrefix() const {
	return "    "
		   "    ";
}