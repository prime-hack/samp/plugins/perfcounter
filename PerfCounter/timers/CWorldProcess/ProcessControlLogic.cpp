#include "ProcessControlLogic.h"

ProcessControlLogic::ProcessControlLogic( SRDescent *parent ) : IPerfTimer( parent ) {
	hook_before.onBefore += [this] {
		before();
	};
	hook_before.install();
	hook_after.onBefore += [this] {
		after();
	};
	hook_after.install();
}

std::string_view ProcessControlLogic::getName() const {
	return "inline::ProcessControlLogic";
}

std::string_view ProcessControlLogic::getPrefix() const {
	return "    "
		   "    ";
}