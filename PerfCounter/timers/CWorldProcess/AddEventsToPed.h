#pragma once

#include "CWorldProcess.h"
#include <llmo/SRHookFast.h>

class AddEventsToPed : public IPerfTimer {
public:
	AddEventsToPed( CWorldProcess *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
