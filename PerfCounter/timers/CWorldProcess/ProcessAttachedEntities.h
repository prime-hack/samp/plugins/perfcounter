#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class ProcessAttachedEntities : public IPerfTimer {
	SRHook::Fast::Hook hook_before{ 0x5689D9, 5 };
	SRHook::Fast::Hook hook_after{ 0x568A71, 5 };

public:
	ProcessAttachedEntities( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
