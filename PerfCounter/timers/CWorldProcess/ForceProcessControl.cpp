#include "ForceProcessControl.h"

ForceProcessControl::ForceProcessControl( SRDescent *parent ) : IPerfTimer( parent ) {
	hook_before.onBefore += [this] {
		before();
	};
	hook_before.install();
	hook_after.onBefore += [this] {
		after();
	};
	hook_after.install();
}

std::string_view ForceProcessControl::getName() const {
	return "inline::ForceProcessControl";
}

std::string_view ForceProcessControl::getPrefix() const {
	return "    "
		   "    ";
}