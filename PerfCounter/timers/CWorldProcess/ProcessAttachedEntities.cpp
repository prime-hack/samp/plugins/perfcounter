#include "ProcessAttachedEntities.h"

ProcessAttachedEntities::ProcessAttachedEntities( SRDescent *parent ) : IPerfTimer( parent ) {
	hook_before.onBefore += [this] {
		before();
	};
	hook_before.install();
	hook_after.onBefore += [this] {
		after();
	};
	hook_after.install();
}

std::string_view ProcessAttachedEntities::getName() const {
	return "inline::ProcessAttachedEntities";
}

std::string_view ProcessAttachedEntities::getPrefix() const {
	return "    "
		   "    ";
}