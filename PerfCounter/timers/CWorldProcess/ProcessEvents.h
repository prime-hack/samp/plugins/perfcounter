#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class ProcessEvents : public IPerfTimer {
	SRHook::Fast::Hook hook_before{ 0x568506, 5 };
	SRHook::Fast::Hook hook_after{ 0x568519, 5 };

public:
	ProcessEvents( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
