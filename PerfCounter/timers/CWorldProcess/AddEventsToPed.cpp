#include "AddEventsToPed.h"

AddEventsToPed::AddEventsToPed( CWorldProcess *parent ) : IPerfTimer( parent ) {
	parent->onBefore += [this] {
		before();
	};
	parent->onGrpEvtLoop += [this] {
		after();
	};
}

std::string_view AddEventsToPed::getName() const {
	return "inline::AddEventsToPed";
}

std::string_view AddEventsToPed::getPrefix() const {
	return "    "
		   "    ";
}