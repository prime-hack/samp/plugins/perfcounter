#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class Cutscene : public IPerfTimer {
	SRHook::Fast::Hook hook_before{ 0x56852C, 5 };
	SRHook::Fast::Hook hook_after{ 0x5685A1, 6 };

public:
	Cutscene( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
