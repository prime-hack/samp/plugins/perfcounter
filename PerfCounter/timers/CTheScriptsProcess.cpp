#include "CTheScriptsProcess.h"

CTheScriptsProcess::CTheScriptsProcess( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CTheScriptsProcess::getName() const {
	return "CTheScripts::Process";
}

std::string_view CTheScriptsProcess::getPrefix() const {
	return "    ";
}
