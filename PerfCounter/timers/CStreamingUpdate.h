#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CStreamingUpdate : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53BF0B, 5 };

public:
	CStreamingUpdate( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
