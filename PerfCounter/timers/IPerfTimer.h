#pragma once

#include <SRDescent/SRDescent.h>
#include <chrono>
#include <vector>
#include <string_view>

class IPerfTimer : public SRDescent {
public:
	static constexpr auto kUpdateTime = std::chrono::seconds( 1 );

	IPerfTimer() = delete;
	IPerfTimer( SRDescent *parent );

	[[nodiscard]] static std::chrono::nanoseconds current_time();

	std::chrono::nanoseconds getTime() const;

	virtual std::string_view getName() const;
	virtual std::string_view getPrefix() const;

protected:
	mutable std::chrono::nanoseconds lastUpdate = std::chrono::nanoseconds( 0 );
	mutable std::vector<std::chrono::nanoseconds> time_accumulate;
	mutable std::chrono::nanoseconds time = std::chrono::nanoseconds( 0 );

	std::chrono::nanoseconds begin = std::chrono::nanoseconds( 0 );
	void before();
	void after();
};

#define SRHOOK_CTOR( ... ) \
	hook.onBefore += [this] { \
		IPerfTimer::before(); \
	}; \
	hook.onAfter += [this] { \
		IPerfTimer::after(); \
	}; \
	hook.install( __VA_ARGS__ );
