#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CGameProcess : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53E981, 5 };

public:
	CGameProcess( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
