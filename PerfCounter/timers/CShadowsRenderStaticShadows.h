#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CShadowsRenderStaticShadows : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53E0C3, 5 };

public:
	CShadowsRenderStaticShadows( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
