#include "CWorldProcessPedsAfterPreRender.h"

CWorldProcessPedsAfterPreRender::CWorldProcessPedsAfterPreRender( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CWorldProcessPedsAfterPreRender::getName() const {
	return "CWorld::ProcessPedsAfterPreRender";
}

std::string_view CWorldProcessPedsAfterPreRender::getPrefix() const {
	return "  ";
}
