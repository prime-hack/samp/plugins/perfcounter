#include "CAudioEngineService.h"

CAudioEngineService::CAudioEngineService( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CAudioEngineService::getName() const {
	return "CAudioEngine::Service";
}

std::string_view CAudioEngineService::getPrefix() const {
	return "  ";
}
