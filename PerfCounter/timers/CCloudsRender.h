#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CCloudsRender : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53DFA0, 5 };

public:
	CCloudsRender( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
