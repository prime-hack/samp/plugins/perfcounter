#include "CRendererPreRender.h"

CRendererPreRender::CRendererPreRender( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CRendererPreRender::getName() const {
	return "CRenderer::PreRender";
}

std::string_view CRendererPreRender::getPrefix() const {
	return "  ";
}
