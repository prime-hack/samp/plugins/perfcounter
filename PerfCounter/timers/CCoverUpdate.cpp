#include "CCoverUpdate.h"

CCoverUpdate::CCoverUpdate( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CCoverUpdate::getName() const {
	return "CCover::Update";
}

std::string_view CCoverUpdate::getPrefix() const {
	return "    ";
}
