#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CCoronasRenderReflections : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53DFD8, 5 };

public:
	CCoronasRenderReflections( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
