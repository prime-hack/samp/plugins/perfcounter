#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CRendererPreRender : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53E9FE, 5 };

public:
	CRendererPreRender( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
