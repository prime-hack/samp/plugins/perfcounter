#include "CRealTimeShadowManagerUpdate.h"

CRealTimeShadowManagerUpdate::CRealTimeShadowManagerUpdate( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CRealTimeShadowManagerUpdate::getName() const {
	return "CRealTimeShadowManager::Update";
}

std::string_view CRealTimeShadowManagerUpdate::getPrefix() const {
	return "  ";
}
