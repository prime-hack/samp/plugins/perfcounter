#include "CWorldProcess.h"

CWorldProcess::CWorldProcess( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();

	hook.onBefore += [this] {
		onBefore();
	};

	grpEvtLoop.onBefore += [this] {
		onGrpEvtLoop();
	};

	grpEvtLoop.install();
}

std::string_view CWorldProcess::getName() const {
	return "CWorld::Process";
}

std::string_view CWorldProcess::getPrefix() const {
	return "    ";
}
