#include "CPlantMgrRender.h"

CPlantMgrRender::CPlantMgrRender( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CPlantMgrRender::getName() const {
	return "CPlantMgr::Render";
}

std::string_view CPlantMgrRender::getPrefix() const {
	return "    ";
}
