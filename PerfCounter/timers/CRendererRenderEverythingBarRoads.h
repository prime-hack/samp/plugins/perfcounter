#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CRendererRenderEverythingBarRoads : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53DFDD, 5 };

public:
	CRendererRenderEverythingBarRoads( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
