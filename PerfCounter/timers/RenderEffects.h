#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class RenderEffects : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53EAD3, 5 };

public:
	RenderEffects( SRDescent *parent );

	std::string_view getPrefix() const override;
};
