#include "IPerfTimer.h"
#ifdef __GNUC__
#	include <cxxabi.h>
#endif

IPerfTimer::IPerfTimer( SRDescent *parent ) : SRDescent( parent ) {}

std::chrono::nanoseconds IPerfTimer::current_time() {
	return std::chrono::duration_cast<std::chrono::nanoseconds>( std::chrono::system_clock::now().time_since_epoch() );
}

std::chrono::nanoseconds IPerfTimer::getTime() const {
	if ( current_time() - lastUpdate > kUpdateTime && !time_accumulate.empty() ) {
		lastUpdate = current_time();
		std::chrono::nanoseconds sum( 0 );
		for ( auto &&time : time_accumulate ) sum += time;
		time = sum / time_accumulate.size();
		time_accumulate.clear();
	}
	return time;
}

std::string_view IPerfTimer::getName() const {
#ifdef __GNUC__
	int status;
	return abi::__cxa_demangle( typeid( *this ).name(), 0, 0, &status );
#else
	return typeid( *this ).name();
#endif
}

std::string_view IPerfTimer::getPrefix() const {
	return "";
}

void IPerfTimer::before() {
	begin = current_time();
}

void IPerfTimer::after() {
	time_accumulate.push_back( current_time() - begin );
}
