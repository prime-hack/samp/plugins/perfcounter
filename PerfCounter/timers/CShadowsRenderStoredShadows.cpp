#include "CShadowsRenderStoredShadows.h"

CShadowsRenderStoredShadows::CShadowsRenderStoredShadows( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CShadowsRenderStoredShadows::getName() const {
	return "CShadows::RenderStoredShadows";
}

std::string_view CShadowsRenderStoredShadows::getPrefix() const {
	return "    ";
}
