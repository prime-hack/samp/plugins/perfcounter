#include "CCoronasRenderReflections.h"

CCoronasRenderReflections::CCoronasRenderReflections( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CCoronasRenderReflections::getName() const {
	return "CCoronas::RenderReflections";
}

std::string_view CCoronasRenderReflections::getPrefix() const {
	return "    ";
}
