#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class Idle : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53ECBD, 5 };

public:
	Idle( SRDescent *parent );
};
