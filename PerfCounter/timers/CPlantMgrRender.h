#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CPlantMgrRender : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53E103, 5 };

public:
	CPlantMgrRender( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
