#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CWorldProcess : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53C095, 5 };
	SRHook::Fast::Hook grpEvtLoop{ 0x5684D3, 5 };

public:
	CWorldProcess( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;

	SRSignal<> onBefore;
	SRSignal<> onGrpEvtLoop;
};
