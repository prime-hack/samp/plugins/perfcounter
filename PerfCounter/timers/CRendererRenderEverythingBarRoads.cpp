#include "CRendererRenderEverythingBarRoads.h"

CRendererRenderEverythingBarRoads::CRendererRenderEverythingBarRoads( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CRendererRenderEverythingBarRoads::getName() const {
	return "CRenderer::RenderEverythingBarRoads";
}

std::string_view CRendererRenderEverythingBarRoads::getPrefix() const {
	return "    ";
}
