#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CMirrorsRenderMirrorBuffer : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53EABA, 5 };

public:
	CMirrorsRenderMirrorBuffer( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
