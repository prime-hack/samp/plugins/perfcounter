#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CRendererConstructRenderList : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53E9F9, 5 };

public:
	CRendererConstructRenderList( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
