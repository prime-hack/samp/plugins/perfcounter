#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CWorldProcessPedsAfterPreRender : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53EA03, 5 };

public:
	CWorldProcessPedsAfterPreRender( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
