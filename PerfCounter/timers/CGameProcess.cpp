#include "CGameProcess.h"

CGameProcess::CGameProcess( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CGameProcess::getName() const {
	return "CGame::Process";
}

std::string_view CGameProcess::getPrefix() const {
	return "  ";
}
