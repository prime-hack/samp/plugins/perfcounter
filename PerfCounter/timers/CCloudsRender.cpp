#include "CCloudsRender.h"

CCloudsRender::CCloudsRender( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CCloudsRender::getName() const {
	return "CClouds::Render";
}

std::string_view CCloudsRender::getPrefix() const {
	return "    ";
}
