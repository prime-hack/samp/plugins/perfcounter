#include "CStreamingUpdate.h"

CStreamingUpdate::CStreamingUpdate( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CStreamingUpdate::getName() const {
	return "CStreaming::Update";
}

std::string_view CStreamingUpdate::getPrefix() const {
	return "    ";
}
