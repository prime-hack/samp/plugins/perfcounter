#include "CMirrorsBeforeMainRender.h"

CMirrorsBeforeMainRender::CMirrorsBeforeMainRender( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CMirrorsBeforeMainRender::getName() const {
	return "CMirrors::BeforeMainRender";
}

std::string_view CMirrorsBeforeMainRender::getPrefix() const {
	return "  ";
}
