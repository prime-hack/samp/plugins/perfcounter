#include "UserTimer.h"
#include "../loader/loader.h"

#include <fstream>
#include <json/single_include/nlohmann/json.hpp>

UserTimer::UserTimer( SRDescent *parent, std::string name, int space, size_t address, ssize_t size, const std::string &module ) :
	IPerfTimer( parent ), name( std::move( name ) ), prefix( space, ' ' ) {
	hook.changeHook( address, size, module );
	SRHOOK_CTOR();
}

std::string_view UserTimer::getName() const {
	return name;
}

std::string_view UserTimer::getPrefix() const {
	return prefix;
}

struct UserTimerHook_t {
	size_t address = 0;
	ssize_t size = 0;
	std::string module;
};

void from_json( const nlohmann::json &j, UserTimerHook_t &uth ) {
	j.at( "address" ).get_to( uth.address );
	j.at( "size" ).get_to( uth.size );
	j.at( "module" ).get_to( uth.module );
}

void createTimer( std::vector<IPerfTimer *> &list, SRDescent *parent, const nlohmann::json &j, int space = 0 ) {
	for ( auto &&[name, value] : j.items() ) {
		if ( name.empty() || value.empty() ) continue;

		try {
			auto uth = value.get<UserTimerHook_t>();
			if ( !uth.address || uth.size < 5 ) continue;
			list.push_back( new UserTimer( parent, name, space, uth.address, uth.size, uth.module ) );
		} catch ( nlohmann::json::exception &exception ) { MessageBox( exception.what(), name ); }
		try {
			auto c = value["children"];
			createTimer( list, parent, c, space + 2 );
		} catch ( ... ) {}
	}
};

void read_json( std::vector<IPerfTimer *> &list, SRDescent *parent ) {
	std::ifstream file( "PerfCounter.json" );
	if ( !file.is_open() ) return;
	nlohmann::json json;
	try {
		file >> json;
	} catch ( nlohmann::json::exception &exception ) { MessageBox( exception.what() ); }
	file.close();

	createTimer( list, parent, json );
}
