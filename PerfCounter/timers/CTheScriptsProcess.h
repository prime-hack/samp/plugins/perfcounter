#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class CTheScriptsProcess : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53BFC7, 5 };

public:
	CTheScriptsProcess( SRDescent *parent );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};
