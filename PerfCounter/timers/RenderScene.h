#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class RenderScene : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53EABF, 5 };

public:
	RenderScene( SRDescent *parent );

	std::string_view getPrefix() const override;
};
