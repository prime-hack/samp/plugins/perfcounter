#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class UserTimer : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0, 5 };
	std::string name;
	std::string prefix;

public:
	UserTimer( SRDescent *parent, std::string name, int space, size_t address, ssize_t size, const std::string &module );

	std::string_view getName() const override;
	std::string_view getPrefix() const override;
};

void read_json( std::vector<IPerfTimer *> &list, SRDescent *parent );
