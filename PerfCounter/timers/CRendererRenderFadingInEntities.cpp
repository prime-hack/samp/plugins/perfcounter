#include "CRendererRenderFadingInEntities.h"

CRendererRenderFadingInEntities::CRendererRenderFadingInEntities( SRDescent *parent ) : IPerfTimer( parent ) {
	SRHOOK_CTOR();
}

std::string_view CRendererRenderFadingInEntities::getName() const {
	return "CRenderer::RenderFadingInEntities";
}

std::string_view CRendererRenderFadingInEntities::getPrefix() const {
	return "    ";
}
