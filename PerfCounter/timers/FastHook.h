#pragma once

#include "IPerfTimer.h"
#include <llmo/SRHookFast.h>

class FastHook : public IPerfTimer {
	SRHook::Fast::Hook hook{ 0x53BEEB, 5 };

public:
	FastHook( SRDescent *parent );

	std::string_view getName() const override;
};
