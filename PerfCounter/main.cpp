#include "main.h"
#include <render/d3drender.h>

#include "timers/Idle.h"
#include "timers/CGameProcess.h"
#include "timers/CStreamingUpdate.h"
#include "timers/CCoverUpdate.h"
#include "timers/CTheScriptsProcess.h"
#include "timers/CAudioEngineService.h"
#include "timers/CRendererConstructRenderList.h"
#include "timers/CRendererPreRender.h"
#include "timers/CWorldProcess.h"
#include "timers/CWorldProcess/AddEventsToPed.h"
#include "timers/CWorldProcess/AddEventsToGrp.h"
#include "timers/CWorldProcess/ProcessEvents.h"
#include "timers/CWorldProcess/PruneRefs.h"
#include "timers/CWorldProcess/Cutscene.h"
#include "timers/CWorldProcess/UpdateAnims.h"
#include "timers/CWorldProcess/ProcessControl.h"
#include "timers/CWorldProcess/ForceProcessControl.h"
#include "timers/CWorldProcess/ProcessControlLogic.h"
#include "timers/CWorldProcess/ProcessPhysics.h"
#include "timers/CWorldProcess/ProcessAttachedEntities.h"
#include "timers/CWorldProcess/ProcessPlayers.h"
#include "timers/CWorldProcessPedsAfterPreRender.h"
#include "timers/CRealTimeShadowManagerUpdate.h"
#include "timers/CMirrorsBeforeMainRender.h"
#include "timers/CMirrorsRenderMirrorBuffer.h"
#include "timers/RenderScene.h"
#include "timers/CCloudsRender.h"
#include "timers/CCoronasRenderReflections.h"
#include "timers/CRendererRenderEverythingBarRoads.h"
#include "timers/CRendererRenderFadingInEntities.h"
#include "timers/CShadowsRenderStaticShadows.h"
#include "timers/CShadowsRenderStoredShadows.h"
#include "timers/CPlantMgrRender.h"
#include "timers/RenderEffects.h"
#include "timers/Render2dStuff.h"
#include "timers/UserTimer.h"
#include "timers/FastHook.h"

using namespace std::chrono_literals;

AsiPlugin::AsiPlugin() : IPerfTimer( nullptr ) {
	// Constructor

	srhook = new FastHook( this );
	g_class.draw->onDraw += std::tuple{ this, &AsiPlugin::draw };

	timers.push_back( new Idle( this ) );
	{
		timers.push_back( new CGameProcess( this ) );
		{
			timers.push_back( new CStreamingUpdate( this ) );
			timers.push_back( new CCoverUpdate( this ) );
			timers.push_back( new CTheScriptsProcess( this ) );

			auto *world_process = new CWorldProcess( this );
			timers.push_back( world_process );
			{
				timers.push_back( new AddEventsToPed( world_process ) );
				timers.push_back( new AddEventsToGrp( world_process ) );
				timers.push_back( new ProcessEvents( world_process ) );
				timers.push_back( new PruneRefs( world_process ) );
				timers.push_back( new Cutscene( world_process ) );
				timers.push_back( new UpdateAnims( world_process ) );
				timers.push_back( new ProcessControl( world_process ) );
				timers.push_back( new ForceProcessControl( world_process ) );
				timers.push_back( new ProcessControlLogic( world_process ) );
				timers.push_back( new ProcessPhysics( world_process ) );
				timers.push_back( new ProcessAttachedEntities( world_process ) );
				timers.push_back( new ProcessPlayers( world_process ) );
			}
		}
		timers.push_back( new CAudioEngineService( this ) );
		timers.push_back( new CRendererConstructRenderList( this ) );
		timers.push_back( new CRendererPreRender( this ) );
		timers.push_back( new CWorldProcessPedsAfterPreRender( this ) );
		timers.push_back( new CRealTimeShadowManagerUpdate( this ) );
		timers.push_back( new CMirrorsBeforeMainRender( this ) );
		timers.push_back( new CMirrorsRenderMirrorBuffer( this ) );
		timers.push_back( new RenderScene( this ) );
		{
			timers.push_back( new CCloudsRender( this ) );
			timers.push_back( new CCoronasRenderReflections( this ) );
			timers.push_back( new CRendererRenderEverythingBarRoads( this ) );
			timers.push_back( new CRendererRenderFadingInEntities( this ) );
			timers.push_back( new CShadowsRenderStaticShadows( this ) );
			timers.push_back( new CShadowsRenderStoredShadows( this ) );
			timers.push_back( new CPlantMgrRender( this ) );
		}
		timers.push_back( new RenderEffects( this ) );
		timers.push_back( new Render2dStuff( this ) );
	}
	read_json( timers, this );
}


template<class Rep, class Period>
static inline std::string format_time( const std::chrono::duration<Rep, Period> &time, std::string_view colorCode = "" ) {
	if ( time > 1s )
		return std::to_string( time / 1s ) + std::string( colorCode ) + "s";
	else if ( time > 1ms )
		return std::to_string( time / 1ms ) + std::string( colorCode ) + "ms";
	else
		return std::to_string( time.count() ) + std::string( colorCode ) + "ns";
}

void AsiPlugin::draw() {
	before();
	if ( !font ) font = g_class.draw->d3d9_createFont( "Segoe UI", 9 );

	float Y = (float)g_class.params.BackBufferHeight * 0.33333f;
	stat.clear();
	for ( auto &&timer : timers ) {
		auto time = timer->getTime();
		strTime = timer->getName();
		colorizeString( strTime );
		strTime += kDefaultColor;
		strTime += ": " + format_time( time, kSuffixColor );
		stat += std::string( timer->getPrefix() ) + strTime + "\n";
	}

	stat += std::string( kDefaultColor ) + "\n\n\nThis stat: " + format_time( getTime() );
	stat += "\nError per hook (x" + std::to_string( timers.size() ) + "): " + format_time( srhook->getTime() );

	const auto statHeight = font->DrawHeight() * ( timers.size() + 4 ) + 5;
	if ( Y + statHeight > g_class.params.BackBufferHeight ) Y = g_class.params.BackBufferHeight - statHeight;
	font->Print( 5, Y, stat );
	after();
}

void AsiPlugin::colorizeString( std::string &str ) {
	for ( auto pos = 0u;; ) {
		auto sep = str.find( "::", pos );
		if ( sep != std::string::npos ) {
			str.insert( sep + 2, kFuncColor );
			str.insert( sep, kDefaultColor );

			size_t classColorSize = 0;
			if ( sep == pos + kInlinePrefix.size() && str.substr( sep - kInlinePrefix.size(), kInlinePrefix.size() ) == kInlinePrefix ) {
				if ( pos >= kFuncColor.size() && str.substr( pos - kFuncColor.size(), kFuncColor.size() ) == kFuncColor )
					str.replace( pos - kFuncColor.size(), kFuncColor.size(), kInlineColor );
				else
					str.insert( pos, kInlineColor );
				classColorSize = kInlineColor.size();
			} else {
				if ( pos >= kFuncColor.size() && str.substr( pos - kFuncColor.size(), kFuncColor.size() ) == kFuncColor )
					str.replace( pos - kFuncColor.size(), kFuncColor.size(), kClassColor );
				else
					str.insert( pos, kClassColor );
				classColorSize = kClassColor.size();
			}
			pos = sep + 2 + kFuncColor.size() + kInlineColor.size() + classColorSize;
		} else {
			if ( pos >= kFuncColor.size() && str.substr( pos - kFuncColor.size(), kFuncColor.size() ) == kFuncColor ) break;
			str.insert( pos, kFuncColor );
			break;
		}
	}
}
