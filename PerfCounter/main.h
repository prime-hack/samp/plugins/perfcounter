#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>

#include "timers/IPerfTimer.h"

class AsiPlugin : public IPerfTimer {
	static constexpr std::string_view kInlinePrefix = "inline";
	static constexpr std::string_view kDefaultColor = "{ffffff}";
	static constexpr std::string_view kClassColor = "{b24da8}";
	static constexpr std::string_view kInlineColor = "{add8e6}";
	static constexpr std::string_view kFuncColor = "{49c268}";
	static constexpr std::string_view kSuffixColor = "{bababa}";

public:
	explicit AsiPlugin();

protected:
	class CD3DFont *font = nullptr;
	std::vector<IPerfTimer *> timers;
	IPerfTimer *srhook;
	std::string stat;
	std::string strTime;
	void draw();
	static void colorizeString( std::string &str );
};

#endif // MAIN_H
